import './style.css'
import{
	letters,
	words,
	upperString,
	titleString,
	backwardsLetters,
	backwardsWords,
	palindrome
} from './functions.js'


//import javascriptLogo from './javascript.svg'
//import viteLogo from '/vite.svg'
//import { setupCounter } from './counter.js'

document.querySelector('#app').innerHTML = `
  <div>
  <header>
		<h1>Ejercicio 1.2: Frase</h1>
	</header>
	<p>Abre la consola para ver el resultado.</p>
	<footer><small>Diseño Web en Entorno Cliente - Juan Segura</small></footer>
  </div>
`
let frase = prompt("Escribe una frase")
if (frase.length == 0) {
    alert("no has introducido ninguna frase")
} else {
    console.log("son => " + letters(frase) + " letras")
    console.log("son => " + words(frase) + " palabras")
    console.log("La frase en mayusculas => " + upperString(frase))
    console.log("La frase con la primera letra de cada palabra en mayuscula => " + titleString(frase))
    console.log("La frase con las letras alreves => " + backwardsLetters(frase))
    console.log("La frase con las palabras alreves => " + backwardsWords(frase))
    if (palindrome(frase)) {
        console.log("La frase es palindrome")
    } else {
        console.log("La frase no es palindrome")
    }
}

