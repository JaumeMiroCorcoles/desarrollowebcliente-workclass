'use strict'

function letters(cadena) {
return cadena.length
}

function words(cadena) {
let palabras = cadena.split(" ")
return palabras.length
}

function upperString(cadena) {
return cadena.toLocaleUpperCase()
}

function titleString(cadena) {
	let array = cadena.split(" ")
	let arrayFinal = array.map(index => index.charAt(0).toUpperCase() + index.slice(1))
	cadena = arrayFinal.join(" ")
	return cadena
}

function backwardsLetters(cadena) {
	return cadena.split("").reverse().join("")
}

function backwardsWords(cadena) {
	return cadena.split(" ").reverse().join(" ")
}

function palindrome(cadena) {
let cadena1 = backwardsLetters(cadena.replaceAll(" ","").toLowerCase())
cadena = cadena.toLowerCase().replaceAll(" ","")
return cadena == cadena1
cadena.reverse
}

export{
	letters,
	words,
	upperString,
	titleString,
	backwardsLetters,
	backwardsWords,
	palindrome
}
